const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module',()=>{
    context('Function isUserNameValid',()=>{
        it('Function prototype : boolean isUserNameValid(username: String)',()=>{
            expect(validate.isUserNameValid('kob')).to.be.true;
    });
    
    it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร',()=>{
            expect(validate.isUserNameValid('tu')).to.be.false;
        });
        it('ทุกตัวต้องเป็นตัวเล็ก',()=>{
            expect(validate.isUserNameValid('Kob')).to.be.false;
            expect(validate.isUserNameValid('koB')).to.be.false;
        });
        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร',()=>{
            expect(validate.isUserNameValid('kob123456789012')).to.be.true;
            expect(validate.isUserNameValid('kob1234567890123')).to.be.false;
        });
    });
    context('Function isAgeValid', ()=>{
        it('Function prototype : boolean isAgeValid (age: String)', ()=>{
            expect(validate.isAgeValid('18')).to.be.true;
        });

        it('age ต้องเป็นข้อความที่เป็นตัวเลข', ()=>{
            expect(validate.isAgeValid('a')).to.be.false;
        });

        it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', ()=>{
            expect(validate.isAgeValid('17')).to.be.true;
        });
    });

    context('Function isPasswordValid', ()=>{
        it('Function prototype : boolean isUserNameValid(password: String)', ()=>{
            expect(validate.isPasswordValid('!A12345678')).to.be.true;
        });

        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', ()=>{
            expect(validate.isPasswordValid('aaaaaaa')).to.be.false;
        });

        it('ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', ()=>{
            expect(validate.isPasswordValid('a12345678')).to.be.false;
        });
        
        it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', ()=>{
            expect(validate.isPasswordValid('aaaaaaa12')).to.be.false;
        });

        it('ต้องมีอักขระ พิเศษ !@#$%^&*()_+|~-=\`{}[]:";<>?,./ อย่างน้อย 1 ตัว', ()=>{
            expect(validate.isPasswordValid('12345678')).to.be.false;
        });
    });
    context('function isDateValid',()=>{
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)',()=>{
            expect(validate.isDateValid('02/01/2019')).to.be.true
            expect(validate.isDateValid('02/33/2019')).to.be.false
        });
        it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน',()=>{
            expect(validate.isDateValid('02/09/2019')).to.be.true
            expect(validate.isDateValid('02/33/2019')).to.be.false
            expect(validate.isDateValid('02/00/2019')).to.be.false
        });
        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน',()=>{
            expect(validate.isDateValid('01/11/2015')).to.be.true
            expect(validate.isDateValid('01/00/2014')).to.be.false
        });
        it('year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020',()=>{
            expect(validate.isDateValid('11/01/2020')).to.be.true
            expect(validate.isDateValid('11/01/2021')).to.be.false
            expect(validate.isDateValid('11/01/1970')).to.be.true
            expect(validate.isDateValid('11/01/1969')).to.be.false
        });
        it('เดือนแต่ละเดือนมีจำนวนวันต่างกันตามรายการดังต่อไปนี้',()=>{
            expect(validate.isDateValid('01/31/2019')).to.be.true
            expect(validate.isDateValid('02/28/2019')).to.be.true
            expect(validate.isDateValid('03/31/2019')).to.be.true
            expect(validate.isDateValid('04/30/2019')).to.be.true
            expect(validate.isDateValid('05/31/2019')).to.be.true
            expect(validate.isDateValid('06/30/2019')).to.be.true
            expect(validate.isDateValid('07/31/2019')).to.be.true
            expect(validate.isDateValid('08/31/2019')).to.be.true
            expect(validate.isDateValid('09/30/2019')).to.be.true
            expect(validate.isDateValid('10/31/2019')).to.be.true
            expect(validate.isDateValid('11/30/2019')).to.be.true
            expect(validate.isDateValid('12/31/2019')).to.be.true
        });
        it('ในกรณีของปี อธิกสุรทิน ให้คำนวนตามหลักเกณฑ์นี้',()=>{
            expect(validate.isDateValid('02/29/2000')).to.be.true
            expect(validate.isDateValid('02/30/2000')).to.be.false
        });
       


    });

});

